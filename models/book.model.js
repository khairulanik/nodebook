// model defines the schema and initiate database
const mongoose = require('mongoose');

const BookSchema = new mongoose.Schema({
    name: String,
    author: String,
    publishedOn: String,
    genre: Array
});

const Book = mongoose.model('Book', BookSchema);

module.exports = Book;