const jwt = require('jsonwebtoken');

const auth = (req, res, next) => {
    try {
        const {authorization} = req.headers;
        const token = authorization.split(' ')[1];
        const {username, userId} = jwt.verify(token, process.env.JWT_SECRET)
        req.username = username;
        req.userId = userId;
        next();
    } catch(err) {
        console.log(err);
        next("Authentication Failed!")
    }
}

module.exports = auth;