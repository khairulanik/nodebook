const createError = require("http-errors");
const express = require('express');
const router = express.Router();
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/user.model");

router.post('/register', async (req, res, next) => {
  try {
    const hashedPassword = await bcrypt.hash(req.body.password, 10);
    const user = new User({
      username: req.body.username,
      password: hashedPassword
    });
  
    await user.save();
    res.status(200).json({
      message: 'Registration successful!'
    });
  } catch {
    next(createError(500))
  }
});

router.post('/login', async (req, res, next) => {
  try {
    const user = await User.find({username: req.body.username});
    if (user && user.length) {
      const isPasswordValid = await bcrypt.compare(req.body.password, user[0].password);
      if (isPasswordValid) {
        const accessToken = jwt.sign({
          username: user[0].username,
          userId: user[0]._id
        }, process.env.JWT_SECRET, {expiresIn: 3600});
        res.status(200).json({
          "access_token": accessToken,
          "message": "Authentication successful"
        })
      } else {
        throw Error('password mismatch');
      }
    } else {
      throw Error('no user found in db');
    }
  } catch(err) {
    console.log(err);
    next(createError(401));
  }
});

module.exports = router;
