const dotenv = require("dotenv");
const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const nunjucks = require("nunjucks");
const mongoose = require("mongoose");
const scss = require("./middlewares/scss");

const indexRouter = require("./routes/index");
const authRouter = require("./routes/auth");

const app = express();
dotenv.config();
const port = process.env.PORT || 3000;

// connect DB
mongoose.connect("mongodb://localhost/nodebooks", {useNewUrlParser: true, useUnifiedTopology: true})
  .then(() => console.log('DB connected'))
  .catch(err => console.log(err));

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "njk");

// configure Nunjucks with 'views' as templates directory
nunjucks.configure("views", {
  autoescape: true,
  express: app,
});

app.use(
  scss({
    src: __dirname + "/scss", // Input SASS files
    dest: __dirname + "/public", // Output CSS
    debug: true,
    outputStyle: "compressed",
  })
);

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/auth", authRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
})

module.exports = app;
